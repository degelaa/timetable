package com.example.bonifacas.timetable;


import android.widget.ListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataProvider {

    public static HashMap<String, List<String>> getInfo()
    {
        HashMap<String, List<String>> Detales = new HashMap<String, List<String>>();
        List<String> Pirmadienis = new ArrayList<String>();
        Pirmadienis.add("I pamoka");
        Pirmadienis.add("II pamoka");
        Pirmadienis.add("III pamoka");
        Pirmadienis.add("IV pamoka");
        Pirmadienis.add("V pamoka");
        List<String> Antadienis = new ArrayList<String>();
        Antadienis.add("I pamoka");
        Antadienis.add("II pamoka");
        Antadienis.add("III pamoka");
        Antadienis.add("IV pamoka");
        Antadienis.add("V pamoka");
        List<String> Treciadienis = new ArrayList<String>();
        Treciadienis.add("I pamoka");
        Treciadienis.add("II pamoka");
        Treciadienis.add("III pamoka");
        Treciadienis.add("IV pamoka");
        Treciadienis.add("V pamoka");
        List<String> Ketvirtadienis = new ArrayList<String>();
        Ketvirtadienis.add("I pamoka");
        Ketvirtadienis.add("II pamoka");
        Ketvirtadienis.add("III pamoka");
        Ketvirtadienis.add("IV pamoka");
        Ketvirtadienis.add("V pamoka");
        List<String> Penktadienis = new ArrayList<String>();
        Penktadienis.add("I pamoka");
        Penktadienis.add("II pamoka");
        Penktadienis.add("III pamoka");
        Penktadienis.add("IV pamoka");
        Penktadienis.add("V pamoka");

        Detales.put("Pirmadienis", Pirmadienis);
        Detales.put("Antadienis", Antadienis);
        Detales.put("Treciadienis", Treciadienis);
        Detales.put("Ketvirtadienis", Ketvirtadienis);
        Detales.put("Penktadienis", Penktadienis);

        return Detales;



    }

}
