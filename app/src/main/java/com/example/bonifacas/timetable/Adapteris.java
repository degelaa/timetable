package com.example.bonifacas.timetable;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

public class Adapteris extends BaseExpandableListAdapter{

    private Context ctx;
    private HashMap<String, List<String>> kategorijos;
    private List<String> Sarasas;

    public Adapteris(Context ctx, HashMap<String, List<String>> kategorijos, List<String> Sarasas)



    {
        this.ctx = ctx;
        this.kategorijos = kategorijos;
        this.Sarasas = Sarasas;

    }

    @Override
    public int getGroupCount() {
        return Sarasas.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return kategorijos.get(Sarasas.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return Sarasas.get(groupPosition);
    }

    @Override
    public Object getChild(int parent, int child) {
        return kategorijos.get(Sarasas.get(parent)).get(child);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int parent, int child) {
        return child;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int parent, boolean isExpanded, View convertView, ViewGroup parentview) {
        String group_title = (String)getGroup(parent);
        if (convertView == null){
            LayoutInflater inflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflator.inflate(R.layout.parent_layout, parentview, false);
        }
        TextView parent_textview = (TextView) convertView.findViewById(R.id.parent_txt);
        parent_textview.setTypeface(null, Typeface.BOLD);
        parent_textview.setText(group_title);
        return convertView;
    }

    @Override
    public View getChildView(int parent, int child, boolean lastChild, View convertview, ViewGroup parentview) {
        String child_title = (String) getChild(parent,child);
        if (convertview == null){
            LayoutInflater inflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertview = inflator.inflate(R.layout.child_layout, parentview, false);
        }

        TextView child_textview = (TextView) convertview.findViewById(R.id.child_txt);
        child_textview.setText(child_title);


        return convertview;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
