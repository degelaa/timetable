package com.example.bonifacas.timetable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.app.Activity;
import android.widget.ExpandableListView;
import android.view.View;


public class MainActivity extends Activity {

    HashMap<String, List<String>> Kategorijos;
    List<String> Sarasas;
    ExpandableListView Exp_list;
    Adapteris adapter;

    NotificationCompat.Builder notifikacija;
    private static final int uniqueID = 12345;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Exp_list = (ExpandableListView) findViewById(R.id.exp_list);
        Kategorijos = DataProvider.getInfo();
        Sarasas = new ArrayList<String>(Kategorijos.keySet());
        adapter = new Adapteris(this, Kategorijos, Sarasas);
        Exp_list.setAdapter(adapter);
        notifikacija = new NotificationCompat.Builder(this);
        notifikacija.setAutoCancel(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void myButtonClicked (View view){
        notifikacija.setSmallIcon(R.drawable.tradeschoolslogo);
        notifikacija.setTicker("This is the Ticker");
        notifikacija.setContentTitle("Here is the tilte");
        notifikacija.setContentText("I am the body text of your notification");

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notifikacija.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notifikacija.build());

    }

}
